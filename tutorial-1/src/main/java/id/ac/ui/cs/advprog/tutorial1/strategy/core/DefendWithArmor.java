package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithArmor implements DefenseBehavior {
    public String defend(){
        return "Deff with Armor";
    }

    public String getType(){
        return "armor";
    }

    //ToDo: Complete me
}
