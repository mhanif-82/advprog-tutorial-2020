package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public interface AttackBehavior extends Strategy {

    public String attack();
}
