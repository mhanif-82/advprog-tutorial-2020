package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithGun implements AttackBehavior {
    public String attack(){
        return "Attacking w/ Gun";
    }

    public String getType(){
        return "gun";
    }

    //ToDo: Complete me
}
